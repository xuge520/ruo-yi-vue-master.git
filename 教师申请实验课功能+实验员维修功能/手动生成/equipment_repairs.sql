/*
Navicat MySQL Data Transfer

Source Server         : localhost_3308
Source Server Version : 80018
Source Host           : localhost:3308
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2024-04-26 17:55:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for equipment_repairs
-- ----------------------------
DROP TABLE IF EXISTS `equipment_repairs`;
CREATE TABLE `equipment_repairs` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `issue_description` text,
  `repair_status` varchar(20) DEFAULT NULL,
  `repair_date` date DEFAULT NULL,
  `repair_illustrate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`repair_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of equipment_repairs
-- ----------------------------
INSERT INTO `equipment_repairs` VALUES ('1', '101', '532', '计算机无法启动', '已维修', '2024-04-15', '1');
INSERT INTO `equipment_repairs` VALUES ('2', '101', '1', '1', '未维修', '2024-05-01', '2');
INSERT INTO `equipment_repairs` VALUES ('3', '123', '123', '666', '未维修', '2024-05-01', '3');
INSERT INTO `equipment_repairs` VALUES ('4', '6', '6', '6', '未维修', '2024-04-11', '4');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表', '3', '1', 'teacherApplication', 'teacherApplication/teacherApplication/index', 1, 0, 'C', '0', '0', 'teacherApplication:teacherApplication:list', '#', 'admin', sysdate(), '', null, '实验室设备报修表菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'teacherApplication:teacherApplication:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'teacherApplication:teacherApplication:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'teacherApplication:teacherApplication:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'teacherApplication:teacherApplication:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师实验室设备报修表导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'teacherApplication:teacherApplication:export',       '#', 'admin', sysdate(), '', null, '');
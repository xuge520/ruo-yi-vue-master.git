/*
Navicat MySQL Data Transfer

Source Server         : localhost_3308
Source Server Version : 80018
Source Host           : localhost:3308
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2024-04-26 21:43:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for labborrowingapproval
-- ----------------------------
DROP TABLE IF EXISTS `labborrowingapproval`;
CREATE TABLE `labborrowingapproval` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(100) DEFAULT NULL,
  `lab_name` varchar(100) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `approval_status` enum('pending','approved','rejected') DEFAULT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of labborrowingapproval
-- ----------------------------
INSERT INTO `labborrowingapproval` VALUES ('1', '李同学', '化学实验室', '化学实验项目', '2024-05-01 08:00:00', '2024-05-01 12:00:00', 'pending');

package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Semester;

/**
 * 学期Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface SemesterMapper 
{
    /**
     * 查询学期
     * 
     * @param id 学期主键
     * @return 学期
     */
    public Semester selectSemesterById(Long id);

    /**
     * 查询学期列表
     * 
     * @param semester 学期
     * @return 学期集合
     */
    public List<Semester> selectSemesterList(Semester semester);

    /**
     * 新增学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int insertSemester(Semester semester);

    /**
     * 修改学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int updateSemester(Semester semester);

    /**
     * 删除学期
     * 
     * @param id 学期主键
     * @return 结果
     */
    public int deleteSemesterById(Long id);

    /**
     * 批量删除学期
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSemesterByIds(Long[] ids);
}

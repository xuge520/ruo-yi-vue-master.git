package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Labborrowingapproval;

/**
 * 实验室借用审批表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface LabborrowingapprovalMapper 
{
    /**
     * 查询实验室借用审批表
     * 
     * @param applicationId 实验室借用审批表主键
     * @return 实验室借用审批表
     */
    public Labborrowingapproval selectLabborrowingapprovalByApplicationId(Long applicationId);

    /**
     * 查询实验室借用审批表列表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 实验室借用审批表集合
     */
    public List<Labborrowingapproval> selectLabborrowingapprovalList(Labborrowingapproval labborrowingapproval);

    /**
     * 新增实验室借用审批表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 结果
     */
    public int insertLabborrowingapproval(Labborrowingapproval labborrowingapproval);

    /**
     * 修改实验室借用审批表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 结果
     */
    public int updateLabborrowingapproval(Labborrowingapproval labborrowingapproval);

    /**
     * 删除实验室借用审批表
     * 
     * @param applicationId 实验室借用审批表主键
     * @return 结果
     */
    public int deleteLabborrowingapprovalByApplicationId(Long applicationId);

    /**
     * 批量删除实验室借用审批表
     * 
     * @param applicationIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLabborrowingapprovalByApplicationIds(Long[] applicationIds);
}

package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Lab;

/**
 * 实验室Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface LabMapper 
{
    /**
     * 查询实验室
     * 
     * @param id 实验室主键
     * @return 实验室
     */
    public Lab selectLabById(Long id);

    /**
     * 查询实验室列表
     * 
     * @param lab 实验室
     * @return 实验室集合
     */
    public List<Lab> selectLabList(Lab lab);

    /**
     * 新增实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    public int insertLab(Lab lab);

    /**
     * 修改实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    public int updateLab(Lab lab);

    /**
     * 删除实验室
     * 
     * @param id 实验室主键
     * @return 结果
     */
    public int deleteLabById(Long id);

    /**
     * 批量删除实验室
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLabByIds(Long[] ids);
}

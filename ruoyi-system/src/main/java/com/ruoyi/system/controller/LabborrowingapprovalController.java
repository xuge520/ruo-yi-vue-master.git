package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Labborrowingapproval;
import com.ruoyi.system.service.ILabborrowingapprovalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验室借用审批表Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/system/labborrowingapproval")
public class LabborrowingapprovalController extends BaseController
{
    @Autowired
    private ILabborrowingapprovalService labborrowingapprovalService;

    /**
     * 查询实验室借用审批表列表
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:list')")
    @GetMapping("/list")
    public TableDataInfo list(Labborrowingapproval labborrowingapproval)
    {
        startPage();
        List<Labborrowingapproval> list = labborrowingapprovalService.selectLabborrowingapprovalList(labborrowingapproval);
        return getDataTable(list);
    }

    /**
     * 导出实验室借用审批表列表
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:export')")
    @Log(title = "实验室借用审批表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Labborrowingapproval labborrowingapproval)
    {
        List<Labborrowingapproval> list = labborrowingapprovalService.selectLabborrowingapprovalList(labborrowingapproval);
        ExcelUtil<Labborrowingapproval> util = new ExcelUtil<Labborrowingapproval>(Labborrowingapproval.class);
        util.exportExcel(response, list, "实验室借用审批表数据");
    }

    /**
     * 获取实验室借用审批表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:query')")
    @GetMapping(value = "/{applicationId}")
    public AjaxResult getInfo(@PathVariable("applicationId") Long applicationId)
    {
        return success(labborrowingapprovalService.selectLabborrowingapprovalByApplicationId(applicationId));
    }

    /**
     * 新增实验室借用审批表
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:add')")
    @Log(title = "实验室借用审批表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Labborrowingapproval labborrowingapproval)
    {
        return toAjax(labborrowingapprovalService.insertLabborrowingapproval(labborrowingapproval));
    }

    /**
     * 修改实验室借用审批表
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:edit')")
    @Log(title = "实验室借用审批表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Labborrowingapproval labborrowingapproval)
    {
        return toAjax(labborrowingapprovalService.updateLabborrowingapproval(labborrowingapproval));
    }

    /**
     * 删除实验室借用审批表
     */
    @PreAuthorize("@ss.hasPermi('system:labborrowingapproval:remove')")
    @Log(title = "实验室借用审批表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{applicationIds}")
    public AjaxResult remove(@PathVariable Long[] applicationIds)
    {
        return toAjax(labborrowingapprovalService.deleteLabborrowingapprovalByApplicationIds(applicationIds));
    }
}

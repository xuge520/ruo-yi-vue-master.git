package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LabSchedule;
import com.ruoyi.system.service.ILabScheduleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验室排课Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/system/schedule")
public class LabScheduleController extends BaseController
{
    @Autowired
    private ILabScheduleService labScheduleService;

    /**
     * 查询实验室排课列表
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:list')")
    @GetMapping("/list")
    public TableDataInfo list(LabSchedule labSchedule)
    {
        startPage();
        List<LabSchedule> list = labScheduleService.selectLabScheduleList(labSchedule);
        return getDataTable(list);
    }

    /**
     * 导出实验室排课列表
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:export')")
    @Log(title = "实验室排课", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LabSchedule labSchedule)
    {
        List<LabSchedule> list = labScheduleService.selectLabScheduleList(labSchedule);
        ExcelUtil<LabSchedule> util = new ExcelUtil<LabSchedule>(LabSchedule.class);
        util.exportExcel(response, list, "实验室排课数据");
    }

    /**
     * 获取实验室排课详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(labScheduleService.selectLabScheduleById(id));
    }

    /**
     * 新增实验室排课
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:add')")
    @Log(title = "实验室排课", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LabSchedule labSchedule)
    {
        return toAjax(labScheduleService.insertLabSchedule(labSchedule));
    }

    /**
     * 修改实验室排课
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:edit')")
    @Log(title = "实验室排课", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LabSchedule labSchedule)
    {
        return toAjax(labScheduleService.updateLabSchedule(labSchedule));
    }

    /**
     * 删除实验室排课
     */
    @PreAuthorize("@ss.hasPermi('system:schedule:remove')")
    @Log(title = "实验室排课", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(labScheduleService.deleteLabScheduleByIds(ids));
    }
}

package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TApplication;
import com.ruoyi.system.service.ITApplicationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 教师排课申请Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/system/t_application")
public class TApplicationController extends BaseController
{
    @Autowired
    private ITApplicationService tApplicationService;

    /**
     * 查询教师排课申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:list')")
    @GetMapping("/list")
    public TableDataInfo list(TApplication tApplication)
    {
        startPage();
        List<TApplication> list = tApplicationService.selectTApplicationList(tApplication);
        return getDataTable(list);
    }

    /**
     * 导出教师排课申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:export')")
    @Log(title = "教师排课申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TApplication tApplication)
    {
        List<TApplication> list = tApplicationService.selectTApplicationList(tApplication);
        ExcelUtil<TApplication> util = new ExcelUtil<TApplication>(TApplication.class);
        util.exportExcel(response, list, "教师排课申请数据");
    }

    /**
     * 获取教师排课申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tApplicationService.selectTApplicationById(id));
    }

    /**
     * 新增教师排课申请
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:add')")
    @Log(title = "教师排课申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TApplication tApplication)
    {
        return toAjax(tApplicationService.insertTApplication(tApplication));
    }

    /**
     * 修改教师排课申请
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:edit')")
    @Log(title = "教师排课申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TApplication tApplication)
    {
        return toAjax(tApplicationService.updateTApplication(tApplication));
    }

    /**
     * 删除教师排课申请
     */
    @PreAuthorize("@ss.hasPermi('system:t_application:remove')")
    @Log(title = "教师排课申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tApplicationService.deleteTApplicationByIds(ids));
    }
}

package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TApplicationMapper;
import com.ruoyi.system.domain.TApplication;
import com.ruoyi.system.service.ITApplicationService;

/**
 * 教师排课申请Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class TApplicationServiceImpl implements ITApplicationService 
{
    @Autowired
    private TApplicationMapper tApplicationMapper;

    /**
     * 查询教师排课申请
     * 
     * @param id 教师排课申请主键
     * @return 教师排课申请
     */
    @Override
    public TApplication selectTApplicationById(Long id)
    {
        return tApplicationMapper.selectTApplicationById(id);
    }

    /**
     * 查询教师排课申请列表
     * 
     * @param tApplication 教师排课申请
     * @return 教师排课申请
     */
    @Override
    public List<TApplication> selectTApplicationList(TApplication tApplication)
    {
        return tApplicationMapper.selectTApplicationList(tApplication);
    }

    /**
     * 新增教师排课申请
     * 
     * @param tApplication 教师排课申请
     * @return 结果
     */
    @Override
    public int insertTApplication(TApplication tApplication)
    {
        return tApplicationMapper.insertTApplication(tApplication);
    }

    /**
     * 修改教师排课申请
     * 
     * @param tApplication 教师排课申请
     * @return 结果
     */
    @Override
    public int updateTApplication(TApplication tApplication)
    {
        return tApplicationMapper.updateTApplication(tApplication);
    }

    /**
     * 批量删除教师排课申请
     * 
     * @param ids 需要删除的教师排课申请主键
     * @return 结果
     */
    @Override
    public int deleteTApplicationByIds(Long[] ids)
    {
        return tApplicationMapper.deleteTApplicationByIds(ids);
    }

    /**
     * 删除教师排课申请信息
     * 
     * @param id 教师排课申请主键
     * @return 结果
     */
    @Override
    public int deleteTApplicationById(Long id)
    {
        return tApplicationMapper.deleteTApplicationById(id);
    }
}

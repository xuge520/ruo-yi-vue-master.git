package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SApplicationMapper;
import com.ruoyi.system.domain.SApplication;
import com.ruoyi.system.service.ISApplicationService;

/**
 * 学生借用申请Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class SApplicationServiceImpl implements ISApplicationService 
{
    @Autowired
    private SApplicationMapper sApplicationMapper;

    /**
     * 查询学生借用申请
     * 
     * @param id 学生借用申请主键
     * @return 学生借用申请
     */
    @Override
    public SApplication selectSApplicationById(Long id)
    {
        return sApplicationMapper.selectSApplicationById(id);
    }

    /**
     * 查询学生借用申请列表
     * 
     * @param sApplication 学生借用申请
     * @return 学生借用申请
     */
    @Override
    public List<SApplication> selectSApplicationList(SApplication sApplication)
    {
        return sApplicationMapper.selectSApplicationList(sApplication);
    }

    /**
     * 新增学生借用申请
     * 
     * @param sApplication 学生借用申请
     * @return 结果
     */
    @Override
    public int insertSApplication(SApplication sApplication)
    {
        return sApplicationMapper.insertSApplication(sApplication);
    }

    /**
     * 修改学生借用申请
     * 
     * @param sApplication 学生借用申请
     * @return 结果
     */
    @Override
    public int updateSApplication(SApplication sApplication)
    {
        return sApplicationMapper.updateSApplication(sApplication);
    }

    /**
     * 批量删除学生借用申请
     * 
     * @param ids 需要删除的学生借用申请主键
     * @return 结果
     */
    @Override
    public int deleteSApplicationByIds(Long[] ids)
    {
        return sApplicationMapper.deleteSApplicationByIds(ids);
    }

    /**
     * 删除学生借用申请信息
     * 
     * @param id 学生借用申请主键
     * @return 结果
     */
    @Override
    public int deleteSApplicationById(Long id)
    {
        return sApplicationMapper.deleteSApplicationById(id);
    }
}

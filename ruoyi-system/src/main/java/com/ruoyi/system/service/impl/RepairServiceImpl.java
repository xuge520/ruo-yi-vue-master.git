package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.RepairMapper;
import com.ruoyi.system.domain.Repair;
import com.ruoyi.system.service.IRepairService;

/**
 * 设备维修Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class RepairServiceImpl implements IRepairService 
{
    @Autowired
    private RepairMapper repairMapper;

    /**
     * 查询设备维修
     * 
     * @param id 设备维修主键
     * @return 设备维修
     */
    @Override
    public Repair selectRepairById(Long id)
    {
        return repairMapper.selectRepairById(id);
    }

    /**
     * 查询设备维修列表
     * 
     * @param repair 设备维修
     * @return 设备维修
     */
    @Override
    public List<Repair> selectRepairList(Repair repair)
    {
        return repairMapper.selectRepairList(repair);
    }

    /**
     * 新增设备维修
     * 
     * @param repair 设备维修
     * @return 结果
     */
    @Override
    public int insertRepair(Repair repair)
    {
        return repairMapper.insertRepair(repair);
    }

    /**
     * 修改设备维修
     * 
     * @param repair 设备维修
     * @return 结果
     */
    @Override
    public int updateRepair(Repair repair)
    {
        return repairMapper.updateRepair(repair);
    }

    /**
     * 批量删除设备维修
     * 
     * @param ids 需要删除的设备维修主键
     * @return 结果
     */
    @Override
    public int deleteRepairByIds(Long[] ids)
    {
        return repairMapper.deleteRepairByIds(ids);
    }

    /**
     * 删除设备维修信息
     * 
     * @param id 设备维修主键
     * @return 结果
     */
    @Override
    public int deleteRepairById(Long id)
    {
        return repairMapper.deleteRepairById(id);
    }
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SApplication;

/**
 * 学生借用申请Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface ISApplicationService 
{
    /**
     * 查询学生借用申请
     * 
     * @param id 学生借用申请主键
     * @return 学生借用申请
     */
    public SApplication selectSApplicationById(Long id);

    /**
     * 查询学生借用申请列表
     * 
     * @param sApplication 学生借用申请
     * @return 学生借用申请集合
     */
    public List<SApplication> selectSApplicationList(SApplication sApplication);

    /**
     * 新增学生借用申请
     * 
     * @param sApplication 学生借用申请
     * @return 结果
     */
    public int insertSApplication(SApplication sApplication);

    /**
     * 修改学生借用申请
     * 
     * @param sApplication 学生借用申请
     * @return 结果
     */
    public int updateSApplication(SApplication sApplication);

    /**
     * 批量删除学生借用申请
     * 
     * @param ids 需要删除的学生借用申请主键集合
     * @return 结果
     */
    public int deleteSApplicationByIds(Long[] ids);

    /**
     * 删除学生借用申请信息
     * 
     * @param id 学生借用申请主键
     * @return 结果
     */
    public int deleteSApplicationById(Long id);
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ExperimentApplications;

/**
 * 实验课申请登记表Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IExperimentApplicationsService 
{
    /**
     * 查询实验课申请登记表
     * 
     * @param applicationId 实验课申请登记表主键
     * @return 实验课申请登记表
     */
    public ExperimentApplications selectExperimentApplicationsByApplicationId(Long applicationId);

    /**
     * 查询实验课申请登记表列表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 实验课申请登记表集合
     */
    public List<ExperimentApplications> selectExperimentApplicationsList(ExperimentApplications experimentApplications);

    /**
     * 新增实验课申请登记表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 结果
     */
    public int insertExperimentApplications(ExperimentApplications experimentApplications);

    /**
     * 修改实验课申请登记表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 结果
     */
    public int updateExperimentApplications(ExperimentApplications experimentApplications);

    /**
     * 批量删除实验课申请登记表
     * 
     * @param applicationIds 需要删除的实验课申请登记表主键集合
     * @return 结果
     */
    public int deleteExperimentApplicationsByApplicationIds(Long[] applicationIds);

    /**
     * 删除实验课申请登记表信息
     * 
     * @param applicationId 实验课申请登记表主键
     * @return 结果
     */
    public int deleteExperimentApplicationsByApplicationId(Long applicationId);
}

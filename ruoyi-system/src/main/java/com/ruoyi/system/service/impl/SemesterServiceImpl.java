package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SemesterMapper;
import com.ruoyi.system.domain.Semester;
import com.ruoyi.system.service.ISemesterService;

/**
 * 学期Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class SemesterServiceImpl implements ISemesterService 
{
    @Autowired
    private SemesterMapper semesterMapper;

    /**
     * 查询学期
     * 
     * @param id 学期主键
     * @return 学期
     */
    @Override
    public Semester selectSemesterById(Long id)
    {
        return semesterMapper.selectSemesterById(id);
    }

    /**
     * 查询学期列表
     * 
     * @param semester 学期
     * @return 学期
     */
    @Override
    public List<Semester> selectSemesterList(Semester semester)
    {
        return semesterMapper.selectSemesterList(semester);
    }

    /**
     * 新增学期
     * 
     * @param semester 学期
     * @return 结果
     */
    @Override
    public int insertSemester(Semester semester)
    {
        return semesterMapper.insertSemester(semester);
    }

    /**
     * 修改学期
     * 
     * @param semester 学期
     * @return 结果
     */
    @Override
    public int updateSemester(Semester semester)
    {
        return semesterMapper.updateSemester(semester);
    }

    /**
     * 批量删除学期
     * 
     * @param ids 需要删除的学期主键
     * @return 结果
     */
    @Override
    public int deleteSemesterByIds(Long[] ids)
    {
        return semesterMapper.deleteSemesterByIds(ids);
    }

    /**
     * 删除学期信息
     * 
     * @param id 学期主键
     * @return 结果
     */
    @Override
    public int deleteSemesterById(Long id)
    {
        return semesterMapper.deleteSemesterById(id);
    }
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TApplication;

/**
 * 教师排课申请Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface ITApplicationService 
{
    /**
     * 查询教师排课申请
     * 
     * @param id 教师排课申请主键
     * @return 教师排课申请
     */
    public TApplication selectTApplicationById(Long id);

    /**
     * 查询教师排课申请列表
     * 
     * @param tApplication 教师排课申请
     * @return 教师排课申请集合
     */
    public List<TApplication> selectTApplicationList(TApplication tApplication);

    /**
     * 新增教师排课申请
     * 
     * @param tApplication 教师排课申请
     * @return 结果
     */
    public int insertTApplication(TApplication tApplication);

    /**
     * 修改教师排课申请
     * 
     * @param tApplication 教师排课申请
     * @return 结果
     */
    public int updateTApplication(TApplication tApplication);

    /**
     * 批量删除教师排课申请
     * 
     * @param ids 需要删除的教师排课申请主键集合
     * @return 结果
     */
    public int deleteTApplicationByIds(Long[] ids);

    /**
     * 删除教师排课申请信息
     * 
     * @param id 教师排课申请主键
     * @return 结果
     */
    public int deleteTApplicationById(Long id);
}

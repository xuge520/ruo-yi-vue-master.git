package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LabborrowingapprovalMapper;
import com.ruoyi.system.domain.Labborrowingapproval;
import com.ruoyi.system.service.ILabborrowingapprovalService;

/**
 * 实验室借用审批表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class LabborrowingapprovalServiceImpl implements ILabborrowingapprovalService 
{
    @Autowired
    private LabborrowingapprovalMapper labborrowingapprovalMapper;

    /**
     * 查询实验室借用审批表
     * 
     * @param applicationId 实验室借用审批表主键
     * @return 实验室借用审批表
     */
    @Override
    public Labborrowingapproval selectLabborrowingapprovalByApplicationId(Long applicationId)
    {
        return labborrowingapprovalMapper.selectLabborrowingapprovalByApplicationId(applicationId);
    }

    /**
     * 查询实验室借用审批表列表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 实验室借用审批表
     */
    @Override
    public List<Labborrowingapproval> selectLabborrowingapprovalList(Labborrowingapproval labborrowingapproval)
    {
        return labborrowingapprovalMapper.selectLabborrowingapprovalList(labborrowingapproval);
    }

    /**
     * 新增实验室借用审批表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 结果
     */
    @Override
    public int insertLabborrowingapproval(Labborrowingapproval labborrowingapproval)
    {
        return labborrowingapprovalMapper.insertLabborrowingapproval(labborrowingapproval);
    }

    /**
     * 修改实验室借用审批表
     * 
     * @param labborrowingapproval 实验室借用审批表
     * @return 结果
     */
    @Override
    public int updateLabborrowingapproval(Labborrowingapproval labborrowingapproval)
    {
        return labborrowingapprovalMapper.updateLabborrowingapproval(labborrowingapproval);
    }

    /**
     * 批量删除实验室借用审批表
     * 
     * @param applicationIds 需要删除的实验室借用审批表主键
     * @return 结果
     */
    @Override
    public int deleteLabborrowingapprovalByApplicationIds(Long[] applicationIds)
    {
        return labborrowingapprovalMapper.deleteLabborrowingapprovalByApplicationIds(applicationIds);
    }

    /**
     * 删除实验室借用审批表信息
     * 
     * @param applicationId 实验室借用审批表主键
     * @return 结果
     */
    @Override
    public int deleteLabborrowingapprovalByApplicationId(Long applicationId)
    {
        return labborrowingapprovalMapper.deleteLabborrowingapprovalByApplicationId(applicationId);
    }
}

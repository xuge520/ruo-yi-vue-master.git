package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实验室对象 lab
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class Lab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 实验室编号 */
    private Long id;

    /** 实验室名称 */
    @Excel(name = "实验室名称")
    private String labName;

    /** 实验室类型 */
    @Excel(name = "实验室类型")
    private String labType;

    /** 设备数 */
    @Excel(name = "设备数")
    private Long eNumber;

    /** 实验员名称 */
    @Excel(name = "实验员名称")
    private String manager;

    /** 实验员id */
    @Excel(name = "实验员id")
    private Long manageId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLabName(String labName) 
    {
        this.labName = labName;
    }

    public String getLabName() 
    {
        return labName;
    }
    public void setLabType(String labType) 
    {
        this.labType = labType;
    }

    public String getLabType() 
    {
        return labType;
    }
    public void seteNumber(Long eNumber) 
    {
        this.eNumber = eNumber;
    }

    public Long geteNumber() 
    {
        return eNumber;
    }
    public void setManager(String manager) 
    {
        this.manager = manager;
    }

    public String getManager() 
    {
        return manager;
    }
    public void setManageId(Long manageId) 
    {
        this.manageId = manageId;
    }

    public Long getManageId() 
    {
        return manageId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("labName", getLabName())
            .append("labType", getLabType())
            .append("eNumber", geteNumber())
            .append("manager", getManager())
            .append("manageId", getManageId())
            .toString();
    }
}

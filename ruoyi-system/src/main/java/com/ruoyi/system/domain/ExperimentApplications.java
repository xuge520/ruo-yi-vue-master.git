package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实验课申请登记表对象 experiment_applications
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public class ExperimentApplications extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 申请唯一标识符 */
    private Long applicationId;

    /** 申请教师的唯一标识符 */
    @Excel(name = "申请教师的唯一标识符")
    private Long teacherId;

    /** 上课学期 */
    @Excel(name = "上课学期")
    private String term;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 需要的实验室类型 */
    @Excel(name = "需要的实验室类型")
    private String labType;

    /** 学生班级 */
    @Excel(name = "学生班级")
    private String className;

    /** 学生人数 */
    @Excel(name = "学生人数")
    private Long studentCount;

    /** 起始周 */
    @Excel(name = "起始周")
    private Long startWeek;

    /** 结束周 */
    @Excel(name = "结束周")
    private Long endWeek;

    /** 节次 */
    @Excel(name = "节次")
    private String classSessions;

    public void setApplicationId(Long applicationId) 
    {
        this.applicationId = applicationId;
    }

    public Long getApplicationId() 
    {
        return applicationId;
    }
    public void setTeacherId(Long teacherId) 
    {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() 
    {
        return teacherId;
    }
    public void setTerm(String term) 
    {
        this.term = term;
    }

    public String getTerm() 
    {
        return term;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setLabType(String labType) 
    {
        this.labType = labType;
    }

    public String getLabType() 
    {
        return labType;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setStudentCount(Long studentCount) 
    {
        this.studentCount = studentCount;
    }

    public Long getStudentCount() 
    {
        return studentCount;
    }
    public void setStartWeek(Long startWeek) 
    {
        this.startWeek = startWeek;
    }

    public Long getStartWeek() 
    {
        return startWeek;
    }
    public void setEndWeek(Long endWeek) 
    {
        this.endWeek = endWeek;
    }

    public Long getEndWeek() 
    {
        return endWeek;
    }
    public void setClassSessions(String classSessions) 
    {
        this.classSessions = classSessions;
    }

    public String getClassSessions() 
    {
        return classSessions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("applicationId", getApplicationId())
            .append("teacherId", getTeacherId())
            .append("term", getTerm())
            .append("courseName", getCourseName())
            .append("labType", getLabType())
            .append("className", getClassName())
            .append("studentCount", getStudentCount())
            .append("startWeek", getStartWeek())
            .append("endWeek", getEndWeek())
            .append("classSessions", getClassSessions())
            .toString();
    }
}

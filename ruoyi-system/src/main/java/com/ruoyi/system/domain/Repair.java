package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备维修对象 repair
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class Repair extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 报修教师 */
    @Excel(name = "报修教师")
    private String tName;

    /** 教师id */
    @Excel(name = "教师id")
    private Long tId;

    /** 实验室编号 */
    @Excel(name = "实验室编号")
    private Long labId;

    /** 故障描述 */
    @Excel(name = "故障描述")
    private String reason;

    /** 填报日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "填报日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 维修状态 */
    @Excel(name = "维修状态")
    private String status;

    /** 维修情况 */
    @Excel(name = "维修情况")
    private String rData;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void settName(String tName) 
    {
        this.tName = tName;
    }

    public String gettName() 
    {
        return tName;
    }
    public void settId(Long tId) 
    {
        this.tId = tId;
    }

    public Long gettId() 
    {
        return tId;
    }
    public void setLabId(Long labId) 
    {
        this.labId = labId;
    }

    public Long getLabId() 
    {
        return labId;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setrData(String rData) 
    {
        this.rData = rData;
    }

    public String getrData() 
    {
        return rData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tName", gettName())
            .append("tId", gettId())
            .append("labId", getLabId())
            .append("reason", getReason())
            .append("date", getDate())
            .append("status", getStatus())
            .append("rData", getrData())
            .toString();
    }
}

package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实验室排课对象 lab_schedule
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class LabSchedule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 排课学期 */
    @Excel(name = "排课学期")
    private String semester;

    /** 实验室名称 */
    @Excel(name = "实验室名称")
    private String labName;

    /** 实验室编号 */
    @Excel(name = "实验室编号")
    private Long labId;

    /** 节次，如“1-2”，“3-5” */
    @Excel(name = "节次，如“1-2”，“3-5”")
    private String session;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 任课教师 */
    @Excel(name = "任课教师")
    private String tname;

    /** 教师id */
    @Excel(name = "教师id")
    private Long tId;

    /** 上课班级 */
    @Excel(name = "上课班级")
    private String uClass;

    /** 星期几 */
    @Excel(name = "星期几")
    private Long day;

    /** 起始周 */
    @Excel(name = "起始周")
    private Long begin;

    /** 结束周 */
    @Excel(name = "结束周")
    private Long end;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSemester(String semester) 
    {
        this.semester = semester;
    }

    public String getSemester() 
    {
        return semester;
    }
    public void setLabName(String labName) 
    {
        this.labName = labName;
    }

    public String getLabName() 
    {
        return labName;
    }
    public void setLabId(Long labId) 
    {
        this.labId = labId;
    }

    public Long getLabId() 
    {
        return labId;
    }
    public void setSession(String session) 
    {
        this.session = session;
    }

    public String getSession() 
    {
        return session;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setTname(String tname) 
    {
        this.tname = tname;
    }

    public String getTname() 
    {
        return tname;
    }
    public void settId(Long tId) 
    {
        this.tId = tId;
    }

    public Long gettId() 
    {
        return tId;
    }
    public void setuClass(String uClass) 
    {
        this.uClass = uClass;
    }

    public String getuClass() 
    {
        return uClass;
    }
    public void setDay(Long day) 
    {
        this.day = day;
    }

    public Long getDay() 
    {
        return day;
    }
    public void setBegin(Long begin) 
    {
        this.begin = begin;
    }

    public Long getBegin() 
    {
        return begin;
    }
    public void setEnd(Long end) 
    {
        this.end = end;
    }

    public Long getEnd() 
    {
        return end;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("semester", getSemester())
            .append("labName", getLabName())
            .append("labId", getLabId())
            .append("session", getSession())
            .append("courseName", getCourseName())
            .append("tname", getTname())
            .append("tId", gettId())
            .append("uClass", getuClass())
            .append("day", getDay())
            .append("begin", getBegin())
            .append("end", getEnd())
            .toString();
    }
}

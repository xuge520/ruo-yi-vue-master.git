package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学期对象 semester
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class Semester extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 年 */
    @Excel(name = "年")
    private String year;

    /** 学期季节 */
    @Excel(name = "学期季节")
    private String season;

    /** 学期总周数 */
    @Excel(name = "学期总周数")
    private String weeks;

    /** 是否为当前学期 */
    @Excel(name = "是否为当前学期")
    private Long current;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setYear(String year) 
    {
        this.year = year;
    }

    public String getYear() 
    {
        return year;
    }
    public void setSeason(String season) 
    {
        this.season = season;
    }

    public String getSeason() 
    {
        return season;
    }
    public void setWeeks(String weeks) 
    {
        this.weeks = weeks;
    }

    public String getWeeks() 
    {
        return weeks;
    }
    public void setCurrent(Long current) 
    {
        this.current = current;
    }

    public Long getCurrent() 
    {
        return current;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("year", getYear())
            .append("season", getSeason())
            .append("weeks", getWeeks())
            .append("current", getCurrent())
            .toString();
    }
}

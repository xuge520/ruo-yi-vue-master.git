package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 教师排课申请对象 t_application
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class TApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 上课学期，默认当前学期 */
    @Excel(name = "上课学期，默认当前学期")
    private String semester;

    /** 申请教师，默认登陆教师 */
    @Excel(name = "申请教师，默认登陆教师")
    private String name;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 实验室类型 */
    @Excel(name = "实验室类型")
    private String labType;

    /** 上课班级 */
    @Excel(name = "上课班级")
    private String upClass;

    /** 学生人数 */
    @Excel(name = "学生人数")
    private Long sNumber;

    /** 起始周 */
    @Excel(name = "起始周")
    private Long begin;

    /** 结束周 */
    @Excel(name = "结束周")
    private Long end;

    /** 节次，如“1-2”，“3-5” */
    @Excel(name = "节次，如“1-2”，“3-5”")
    private String session;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSemester(String semester) 
    {
        this.semester = semester;
    }

    public String getSemester() 
    {
        return semester;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setLabType(String labType) 
    {
        this.labType = labType;
    }

    public String getLabType() 
    {
        return labType;
    }
    public void setUpClass(String upClass) 
    {
        this.upClass = upClass;
    }

    public String getUpClass() 
    {
        return upClass;
    }
    public void setsNumber(Long sNumber) 
    {
        this.sNumber = sNumber;
    }

    public Long getsNumber() 
    {
        return sNumber;
    }
    public void setBegin(Long begin) 
    {
        this.begin = begin;
    }

    public Long getBegin() 
    {
        return begin;
    }
    public void setEnd(Long end) 
    {
        this.end = end;
    }

    public Long getEnd() 
    {
        return end;
    }
    public void setSession(String session) 
    {
        this.session = session;
    }

    public String getSession() 
    {
        return session;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("semester", getSemester())
            .append("name", getName())
            .append("courseName", getCourseName())
            .append("labType", getLabType())
            .append("upClass", getUpClass())
            .append("sNumber", getsNumber())
            .append("begin", getBegin())
            .append("end", getEnd())
            .append("session", getSession())
            .toString();
    }
}

package com.ruoyi.labscheduling.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实验室排课表对象 labscheduling
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public class Labscheduling extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 流水编号 */
    private Long id;

    /** 学期 */
    @Excel(name = "学期")
    private String term;

    /** 实验室类型 */
    @Excel(name = "实验室类型")
    private String labType;

    /** 实验室 */
    @Excel(name = "实验室")
    private String labNum;

    /** 周次 */
    @Excel(name = "周次")
    private String weekNumber;

    /** 星期几 */
    @Excel(name = "星期几")
    private String dateNumber;

    /** 节次 */
    @Excel(name = "节次")
    private String classNumber;

    /** 课程名 */
    @Excel(name = "课程名")
    private String courseName;

    /** 任课教师 */
    @Excel(name = "任课教师")
    private String instructorName;

    /** 学生班级 */
    @Excel(name = "学生班级")
    private String studentClass;

    /** 起始周 */
    @Excel(name = "起始周")
    private String startWeek;

    /** 结束周 */
    @Excel(name = "结束周")
    private String endWeek;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTerm(String term) 
    {
        this.term = term;
    }

    public String getTerm() 
    {
        return term;
    }
    public void setLabType(String labType) 
    {
        this.labType = labType;
    }

    public String getLabType() 
    {
        return labType;
    }
    public void setLabNum(String labNum) 
    {
        this.labNum = labNum;
    }

    public String getLabNum() 
    {
        return labNum;
    }
    public void setWeekNumber(String weekNumber) 
    {
        this.weekNumber = weekNumber;
    }

    public String getWeekNumber() 
    {
        return weekNumber;
    }
    public void setDateNumber(String dateNumber) 
    {
        this.dateNumber = dateNumber;
    }

    public String getDateNumber() 
    {
        return dateNumber;
    }
    public void setClassNumber(String classNumber) 
    {
        this.classNumber = classNumber;
    }

    public String getClassNumber() 
    {
        return classNumber;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setInstructorName(String instructorName) 
    {
        this.instructorName = instructorName;
    }

    public String getInstructorName() 
    {
        return instructorName;
    }
    public void setStudentClass(String studentClass) 
    {
        this.studentClass = studentClass;
    }

    public String getStudentClass() 
    {
        return studentClass;
    }
    public void setStartWeek(String startWeek) 
    {
        this.startWeek = startWeek;
    }

    public String getStartWeek() 
    {
        return startWeek;
    }
    public void setEndWeek(String endWeek) 
    {
        this.endWeek = endWeek;
    }

    public String getEndWeek() 
    {
        return endWeek;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("term", getTerm())
            .append("labType", getLabType())
            .append("labNum", getLabNum())
            .append("weekNumber", getWeekNumber())
            .append("dateNumber", getDateNumber())
            .append("classNumber", getClassNumber())
            .append("courseName", getCourseName())
            .append("instructorName", getInstructorName())
            .append("studentClass", getStudentClass())
            .append("startWeek", getStartWeek())
            .append("endWeek", getEndWeek())
            .toString();
    }
}

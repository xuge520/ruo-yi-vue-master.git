package com.ruoyi.labscheduling.service;

import java.util.List;
import com.ruoyi.labscheduling.domain.Labscheduling;

/**
 * 实验室排课表Service接口
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public interface ILabschedulingService 
{
    /**
     * 查询实验室排课表
     * 
     * @param id 实验室排课表主键
     * @return 实验室排课表
     */
    public Labscheduling selectLabschedulingById(Long id);

    /**
     * 查询实验室排课表列表
     * 
     * @param labscheduling 实验室排课表
     * @return 实验室排课表集合
     */
    public List<Labscheduling> selectLabschedulingList(Labscheduling labscheduling);

    /**
     * 新增实验室排课表
     * 
     * @param labscheduling 实验室排课表
     * @return 结果
     */
    public int insertLabscheduling(Labscheduling labscheduling);

    /**
     * 修改实验室排课表
     * 
     * @param labscheduling 实验室排课表
     * @return 结果
     */
    public int updateLabscheduling(Labscheduling labscheduling);

    /**
     * 批量删除实验室排课表
     * 
     * @param ids 需要删除的实验室排课表主键集合
     * @return 结果
     */
    public int deleteLabschedulingByIds(Long[] ids);

    /**
     * 删除实验室排课表信息
     * 
     * @param id 实验室排课表主键
     * @return 结果
     */
    public int deleteLabschedulingById(Long id);
}

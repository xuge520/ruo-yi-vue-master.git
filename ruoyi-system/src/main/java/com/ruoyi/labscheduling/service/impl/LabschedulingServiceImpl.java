package com.ruoyi.labscheduling.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.labscheduling.mapper.LabschedulingMapper;
import com.ruoyi.labscheduling.domain.Labscheduling;
import com.ruoyi.labscheduling.service.ILabschedulingService;

/**
 * 实验室排课表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@Service
public class LabschedulingServiceImpl implements ILabschedulingService 
{
    @Autowired
    private LabschedulingMapper labschedulingMapper;

    /**
     * 查询实验室排课表
     * 
     * @param id 实验室排课表主键
     * @return 实验室排课表
     */
    @Override
    public Labscheduling selectLabschedulingById(Long id)
    {
        return labschedulingMapper.selectLabschedulingById(id);
    }

    /**
     * 查询实验室排课表列表
     * 
     * @param labscheduling 实验室排课表
     * @return 实验室排课表
     */
    @Override
    public List<Labscheduling> selectLabschedulingList(Labscheduling labscheduling)
    {
        return labschedulingMapper.selectLabschedulingList(labscheduling);
    }

    /**
     * 新增实验室排课表
     * 
     * @param labscheduling 实验室排课表
     * @return 结果
     */
    @Override
    public int insertLabscheduling(Labscheduling labscheduling)
    {
        return labschedulingMapper.insertLabscheduling(labscheduling);
    }

    /**
     * 修改实验室排课表
     * 
     * @param labscheduling 实验室排课表
     * @return 结果
     */
    @Override
    public int updateLabscheduling(Labscheduling labscheduling)
    {
        return labschedulingMapper.updateLabscheduling(labscheduling);
    }

    /**
     * 批量删除实验室排课表
     * 
     * @param ids 需要删除的实验室排课表主键
     * @return 结果
     */
    @Override
    public int deleteLabschedulingByIds(Long[] ids)
    {
        return labschedulingMapper.deleteLabschedulingByIds(ids);
    }

    /**
     * 删除实验室排课表信息
     * 
     * @param id 实验室排课表主键
     * @return 结果
     */
    @Override
    public int deleteLabschedulingById(Long id)
    {
        return labschedulingMapper.deleteLabschedulingById(id);
    }
}

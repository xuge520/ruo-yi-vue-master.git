package com.ruoyi.labscheduling.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.labscheduling.domain.Labscheduling;
import com.ruoyi.labscheduling.service.ILabschedulingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;



/**
 * 实验室排课表Controller
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@RestController
@RequestMapping("/labscheduling/labscheduling")
public class LabschedulingController extends BaseController
{
    @Autowired
    private ILabschedulingService labschedulingService;

    /**
     * 查询实验室排课表列表
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:list')")
    @GetMapping("/list")
    public TableDataInfo list(Labscheduling labscheduling)
    {
        startPage();
        List<Labscheduling> list = labschedulingService.selectLabschedulingList(labscheduling);
        return getDataTable(list);
    }

    /**
     * 导出实验室排课表列表
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:export')")
    @Log(title = "实验室排课表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Labscheduling labscheduling)
    {
        List<Labscheduling> list = labschedulingService.selectLabschedulingList(labscheduling);
        ExcelUtil<Labscheduling> util = new ExcelUtil<Labscheduling>(Labscheduling.class);
        util.exportExcel(response, list, "实验室排课表数据");
    }

    /**
     * 获取实验室排课表详细信息
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(labschedulingService.selectLabschedulingById(id));
    }

    @GetMapping("/getis")
    public long getis(Labscheduling Labscheduling)
    {
        long flagt=1;//1代表没有冲突
        Long tid=Labscheduling.getId();
        String tlabnum=Labscheduling.getLabNum();
        String tdatenum=Labscheduling.getDateNumber();
        String tclassnum=Labscheduling.getClassNumber();
        String tsweek=Labscheduling.getStartWeek();
        String teweek=Labscheduling.getEndWeek();

        List<Labscheduling> labscheduling = labschedulingService.selectLabschedulingList(new Labscheduling());
        for (Labscheduling ls : labscheduling){
            if(tid!=ls.getId()){
                if(tlabnum.equals(ls.getLabNum())&&tdatenum.equals(ls.getDateNumber())&&tclassnum.equals(ls.getClassNumber())){
                    if(Integer.parseInt(tsweek)<=Integer.parseInt(ls.getEndWeek())&&Integer.parseInt(teweek)>=Integer.parseInt(ls.getStartWeek())){
                        flagt=0;
                        break;
                    }
                }
            }
        }
        return flagt;
    }


    //@GetMapping("/labscheduling/labscheduling")
    @GetMapping("/getcourse")
    public AjaxResult getcourse(Labscheduling Labscheduling)
    {
        List<Labscheduling> labscheduling = labschedulingService.selectLabschedulingList(Labscheduling);
        List<JSONObject> list= CollUtil.newArrayList();
        //周一：
        JSONObject jsonObject732_1 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_1 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_1 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_1 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_1 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_1 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_1 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_1 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_1 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_1 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_1 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_1 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_1 = new JSONObject();//计算机网络实验室834

        jsonObject732_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 软件实验室732
        jsonObject733_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 软件实验室733
        jsonObject734_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 软件实验室734
        jsonObject735_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 软件实验室735
        jsonObject703_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机系统实验室703
        jsonObject704_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机系统实验室704
        jsonObject705_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机系统实验室705
        jsonObject706_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机硬件实验室706
        jsonObject707_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机硬件实验室707
        jsonObject835_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 物联网实验室835
        jsonObject836_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 物联网实验室836
        jsonObject832_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机网络实验室832
        jsonObject834_1.put("day", JSONUtil.parseObj("{ \"num\": \"星期一\" }")); // 计算机网络实验室834

        jsonObject732_1.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_1.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_1.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_1.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_1.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_1.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_1.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_1); // 软件实验室732
        list.add(jsonObject733_1); // 软件实验室733
        list.add(jsonObject734_1); // 软件实验室734
        list.add(jsonObject735_1); // 软件实验室735
        list.add(jsonObject703_1); // 计算机系统实验室703
        list.add(jsonObject704_1); // 计算机系统实验室704
        list.add(jsonObject705_1); // 计算机系统实验室705
        list.add(jsonObject706_1); // 计算机硬件实验室706
        list.add(jsonObject707_1); // 计算机硬件实验室707
        list.add(jsonObject835_1); // 物联网实验室835
        list.add(jsonObject836_1); // 物联网实验室836
        list.add(jsonObject832_1); // 计算机网络实验室832
        list.add(jsonObject834_1); // 计算机网络实验室834

//周二:
        JSONObject jsonObject732_2 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_2 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_2 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_2 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_2 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_2 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_2 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_2 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_2 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_2 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_2 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_2 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_2 = new JSONObject();//计算机网络实验室834

        jsonObject732_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 软件实验室732
        jsonObject733_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 软件实验室733
        jsonObject734_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 软件实验室734
        jsonObject735_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 软件实验室735
        jsonObject703_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机系统实验室703
        jsonObject704_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机系统实验室704
        jsonObject705_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机系统实验室705
        jsonObject706_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机硬件实验室706
        jsonObject707_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机硬件实验室707
        jsonObject835_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 物联网实验室835
        jsonObject836_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 物联网实验室836
        jsonObject832_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机网络实验室832
        jsonObject834_2.put("day", JSONUtil.parseObj("{ \"num\": \"星期二\" }")); // 计算机网络实验室8342

        jsonObject732_2.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_2.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_2.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_2.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_2.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_2.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_2.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_2); // 软件实验室732
        list.add(jsonObject733_2); // 软件实验室733
        list.add(jsonObject734_2); // 软件实验室734
        list.add(jsonObject735_2); // 软件实验室735
        list.add(jsonObject703_2); // 计算机系统实验室703
        list.add(jsonObject704_2); // 计算机系统实验室704
        list.add(jsonObject705_2); // 计算机系统实验室705
        list.add(jsonObject706_2); // 计算机硬件实验室706
        list.add(jsonObject707_2); // 计算机硬件实验室707
        list.add(jsonObject835_2); // 物联网实验室835
        list.add(jsonObject836_2); // 物联网实验室836
        list.add(jsonObject832_2); // 计算机网络实验室832
        list.add(jsonObject834_2); // 计算机网络实验室834
//周三:
        JSONObject jsonObject732_3 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_3 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_3 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_3 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_3 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_3 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_3 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_3 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_3 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_3 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_3 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_3 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_3 = new JSONObject();//计算机网络实验室834

        jsonObject732_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 软件实验室732
        jsonObject733_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 软件实验室733
        jsonObject734_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 软件实验室734
        jsonObject735_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 软件实验室735
        jsonObject703_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机系统实验室703
        jsonObject704_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机系统实验室704
        jsonObject705_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机系统实验室705
        jsonObject706_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机硬件实验室706
        jsonObject707_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机硬件实验室707
        jsonObject835_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 物联网实验室835
        jsonObject836_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 物联网实验室836
        jsonObject832_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机网络实验室832
        jsonObject834_3.put("day", JSONUtil.parseObj("{ \"num\": \"星期三\" }")); // 计算机网络实验室834

        jsonObject732_3.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_3.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_3.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_3.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_3.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_3.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_3.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_3); // 软件实验室732
        list.add(jsonObject733_3); // 软件实验室733
        list.add(jsonObject734_3); // 软件实验室734
        list.add(jsonObject735_3); // 软件实验室735
        list.add(jsonObject703_3); // 计算机系统实验室703
        list.add(jsonObject704_3); // 计算机系统实验室704
        list.add(jsonObject705_3); // 计算机系统实验室705
        list.add(jsonObject706_3); // 计算机硬件实验室706
        list.add(jsonObject707_3); // 计算机硬件实验室707
        list.add(jsonObject835_3); // 物联网实验室835
        list.add(jsonObject836_3); // 物联网实验室836
        list.add(jsonObject832_3); // 计算机网络实验室832
        list.add(jsonObject834_3); // 计算机网络实验室834

//周四:
        JSONObject jsonObject732_4 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_4 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_4 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_4 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_4 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_4 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_4 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_4 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_4 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_4 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_4 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_4 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_4 = new JSONObject();//计算机网络实验室8344

        jsonObject732_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 软件实验室732
        jsonObject733_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 软件实验室733
        jsonObject734_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 软件实验室734
        jsonObject735_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 软件实验室735
        jsonObject703_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机系统实验室703
        jsonObject704_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机系统实验室704
        jsonObject705_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机系统实验室705
        jsonObject706_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机硬件实验室706
        jsonObject707_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机硬件实验室707
        jsonObject835_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 物联网实验室835
        jsonObject836_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 物联网实验室836
        jsonObject832_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机网络实验室832
        jsonObject834_4.put("day", JSONUtil.parseObj("{ \"num\": \"星期四\" }")); // 计算机网络实验室834

        jsonObject732_4.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_4.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_4.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_4.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_4.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_4.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_4.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_4); // 软件实验室732
        list.add(jsonObject733_4); // 软件实验室733
        list.add(jsonObject734_4); // 软件实验室734
        list.add(jsonObject735_4); // 软件实验室735
        list.add(jsonObject703_4); // 计算机系统实验室703
        list.add(jsonObject704_4); // 计算机系统实验室704
        list.add(jsonObject705_4); // 计算机系统实验室705
        list.add(jsonObject706_4); // 计算机硬件实验室706
        list.add(jsonObject707_4); // 计算机硬件实验室707
        list.add(jsonObject835_4); // 物联网实验室835
        list.add(jsonObject836_4); // 物联网实验室836
        list.add(jsonObject832_4); // 计算机网络实验室832
        list.add(jsonObject834_4); // 计算机网络实验室834
//周五:
        JSONObject jsonObject732_5 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_5 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_5 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_5 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_5 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_5 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_5 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_5 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_5 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_5 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_5 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_5 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_5 = new JSONObject();//计算机网络实验室834

        jsonObject732_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 软件实验室732
        jsonObject733_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 软件实验室733
        jsonObject734_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 软件实验室734
        jsonObject735_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 软件实验室735
        jsonObject703_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机系统实验室703
        jsonObject704_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机系统实验室704
        jsonObject705_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机系统实验室705
        jsonObject706_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机硬件实验室706
        jsonObject707_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机硬件实验室707
        jsonObject835_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 物联网实验室835
        jsonObject836_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 物联网实验室836
        jsonObject832_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机网络实验室832
        jsonObject834_5.put("day", JSONUtil.parseObj("{ \"num\": \"星期五\" }")); // 计算机网络实验室834

        jsonObject732_5.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_5.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_5.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_5.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_5.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_5.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_5.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_5); // 软件实验室732
        list.add(jsonObject733_5); // 软件实验室733
        list.add(jsonObject734_5); // 软件实验室734
        list.add(jsonObject735_5); // 软件实验室735
        list.add(jsonObject703_5); // 计算机系统实验室703
        list.add(jsonObject704_5); // 计算机系统实验室704
        list.add(jsonObject705_5); // 计算机系统实验室705
        list.add(jsonObject706_5); // 计算机硬件实验室706
        list.add(jsonObject707_5); // 计算机硬件实验室707
        list.add(jsonObject835_5); // 物联网实验室835
        list.add(jsonObject836_5); // 物联网实验室836
        list.add(jsonObject832_5); // 计算机网络实验室832
        list.add(jsonObject834_5); // 计算机网络实验室834

//周六:
        JSONObject jsonObject732_6 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_6 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_6 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_6 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_6 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_6 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_6 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_6 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_6 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_6 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_6 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_6 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_6 = new JSONObject();//计算机网络实验室834

        jsonObject732_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 软件实验室732
        jsonObject733_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 软件实验室733
        jsonObject734_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 软件实验室734
        jsonObject735_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 软件实验室735
        jsonObject703_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机系统实验室703
        jsonObject704_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机系统实验室704
        jsonObject705_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机系统实验室705
        jsonObject706_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机硬件实验室706
        jsonObject707_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机硬件实验室707
        jsonObject835_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 物联网实验室835
        jsonObject836_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 物联网实验室836
        jsonObject832_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机网络实验室832
        jsonObject834_6.put("day", JSONUtil.parseObj("{ \"num\": \"星期六\" }")); // 计算机网络实验室834

        jsonObject732_6.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_6.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_6.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_6.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_6.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_6.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_6.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_6); // 软件实验室732
        list.add(jsonObject733_6); // 软件实验室733
        list.add(jsonObject734_6); // 软件实验室734
        list.add(jsonObject735_6); // 软件实验室735
        list.add(jsonObject703_6); // 计算机系统实验室703
        list.add(jsonObject704_6); // 计算机系统实验室704
        list.add(jsonObject705_6); // 计算机系统实验室705
        list.add(jsonObject706_6); // 计算机硬件实验室706
        list.add(jsonObject707_6); // 计算机硬件实验室707
        list.add(jsonObject835_6); // 物联网实验室835
        list.add(jsonObject836_6); // 物联网实验室836
        list.add(jsonObject832_6); // 计算机网络实验室832
        list.add(jsonObject834_6); // 计算机网络实验室834

//周日:
        JSONObject jsonObject732_7 = new JSONObject();//软件实验室732
        JSONObject jsonObject733_7 = new JSONObject();//软件实验室733
        JSONObject jsonObject734_7 = new JSONObject();//软件实验室734
        JSONObject jsonObject735_7 = new JSONObject();//软件实验室735
        JSONObject jsonObject703_7 = new JSONObject();//计算机系统实验室703
        JSONObject jsonObject704_7 = new JSONObject();//计算机系统实验室704
        JSONObject jsonObject705_7 = new JSONObject();//计算机系统实验室705
        JSONObject jsonObject706_7 = new JSONObject();//计算机硬件实验室706
        JSONObject jsonObject707_7 = new JSONObject();//计算机硬件实验室707
        JSONObject jsonObject835_7 = new JSONObject();//物联网实验室835
        JSONObject jsonObject836_7 = new JSONObject();//物联网实验室836
        JSONObject jsonObject832_7 = new JSONObject();//计算机网络实验室832
        JSONObject jsonObject834_7 = new JSONObject();//计算机网络实验室834

        jsonObject732_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 软件实验室732
        jsonObject733_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 软件实验室733
        jsonObject734_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 软件实验室734
        jsonObject735_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 软件实验室735
        jsonObject703_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机系统实验室703
        jsonObject704_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机系统实验室704
        jsonObject705_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机系统实验室705
        jsonObject706_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机硬件实验室706
        jsonObject707_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机硬件实验室707
        jsonObject835_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 物联网实验室835
        jsonObject836_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 物联网实验室836
        jsonObject832_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机网络实验室832
        jsonObject834_7.put("day", JSONUtil.parseObj("{ \"num\": \"星期天\" }")); // 计算机网络实验室834

        jsonObject732_7.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"732\" }"));
        jsonObject733_7.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"733\" }"));
        jsonObject734_7.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"734\" }"));
        jsonObject735_7.put("lab", JSONUtil.parseObj("{ \"name\": \"软件实验室\",\"number\":\"735\" }"));
        jsonObject703_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"703\" }"));
        jsonObject704_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"704\" }"));
        jsonObject705_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机系统实验室\",\"number\":\"705\" }"));
        jsonObject706_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"706\" }"));
        jsonObject707_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机硬件实验室\",\"number\":\"707\" }"));
        jsonObject835_7.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"835\" }"));
        jsonObject836_7.put("lab", JSONUtil.parseObj("{ \"name\": \"物联网实验室\",\"number\":\"836\" }"));
        jsonObject832_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"832\" }"));
        jsonObject834_7.put("lab", JSONUtil.parseObj("{ \"name\": \"计算机网络实验室\",\"number\":\"834\" }"));

        list.add(jsonObject732_7); // 软件实验室732
        list.add(jsonObject733_7); // 软件实验室733
        list.add(jsonObject734_7); // 软件实验室734
        list.add(jsonObject735_7); // 软件实验室735
        list.add(jsonObject703_7); // 计算机系统实验室703
        list.add(jsonObject704_7); // 计算机系统实验室704
        list.add(jsonObject705_7); // 计算机系统实验室705
        list.add(jsonObject706_7); // 计算机硬件实验室706
        list.add(jsonObject707_7); // 计算机硬件实验室707
        list.add(jsonObject835_7); // 物联网实验室835
        list.add(jsonObject836_7); // 物联网实验室836
        list.add(jsonObject832_7); // 计算机网络实验室832
        list.add(jsonObject834_7); // 计算机网络实验室834
        for (Labscheduling ls : labscheduling){
            String labnumber=ls.getLabNum();
//            String weeks=ls.getWeekNumber();
            String startweek=ls.getStartWeek();
            String endweek=ls.getEndWeek();
            String day=ls.getDateNumber();
            String classnb=ls.getClassNumber();
            String course=ls.getCourseName();
            String instructor=ls.getInstructorName();
            String studentclass=ls.getStudentClass();
            switch (day) {
                case "1" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_1);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_1);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_1);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_1);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_1);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_1);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_1);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_1);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_1);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_1);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_1);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_1);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_1);
                    }
                    break;
                case "2" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_2);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_2);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_2);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_2);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_2);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_2);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_2);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_2);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_2);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_2);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_2);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_2);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_2);
                    }
                    break;
                case "3" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_3);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_3);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_3);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_3);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_3);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_3);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_3);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_3);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_3);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_3);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_3);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_3);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_3);
                    }
                    break;
                case "4" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_4);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_4);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_4);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_4);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_4);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_4);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_4);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_4);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_4);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_4);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_4);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_4);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_4);
                    }
                    break;
                case "5" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_5);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_5);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_5);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_5);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_5);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_5);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_5);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_5);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_5);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_5);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_5);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_5);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_5);
                    }
                    break;
                case "6" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_6);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_6);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_6);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_6);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_6);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_6);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_6);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_6);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_6);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_6);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_6);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_6);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_6);
                    }
                    break;
                case "7" :
                    if ("732".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject732_7);
                    } else if ("733".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject733_7);
                    } else if ("734".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject734_7);
                    } else if ("735".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject735_7);
                    } else if ("703".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject703_7);
                    } else if ("704".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject704_7);
                    } else if ("705".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject705_7);
                    } else if ("706".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject706_7);
                    } else if ("707".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject707_7);
                    } else if ("835".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject835_7);
                    } else if ("836".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject836_7);
                    } else if ("832".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject832_7);
                    } else if ("834".equals(labnumber)) {
                        setjson2(startweek,endweek, classnb, course, instructor, studentclass, jsonObject834_7);
                    }
                    break;
            }

        }
        return AjaxResult.success(list);
    }
    /**
     *根据实验室编号筛选:(实验室1，实验室2，实验室3)
     */
//    private JSONObject setjson1(String labname,String labnumber, String weeks, String classnb,String course,String instructor,String studentclass,JSONObject jsonObject){
//        switch (labnumber){
//            case "实验室1" :
//
//                break;
//            case "实验室2" :
//
//                break;
//            case "实验室3" :
//
//                break;
//        }
//        return jsonObject;
//    }
    /**
     *根据节次筛选，写入数据；
     */
    private JSONObject setjson2(String startweeks,String endweeks, String classnb,String course,String instructor,String studentclass,JSONObject jsonObject){
        JSONObject courseInfo = new JSONObject();
        courseInfo.put("course", course);
        courseInfo.put("teacher", instructor);
        courseInfo.put("week",startweeks+'-'+endweeks);
        courseInfo.put("studentclass", studentclass);
        switch (classnb){
            case "1-2":
                jsonObject.put("one", courseInfo);
                break;
            case "3-5":
                jsonObject.put("two", courseInfo);
                break;
            case "6-7":
                jsonObject.put("three", courseInfo);
                break;
            case "8-9":
                jsonObject.put("four", courseInfo);
                break;
            case "10-12":
                jsonObject.put("five", courseInfo);
                break;
            case "13-15":
                jsonObject.put("six", courseInfo);
                break;
        }
        return jsonObject;
    }



    /**
     * 新增实验室排课表
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:add')")
    @Log(title = "实验室排课表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Labscheduling labscheduling)
    {
        return toAjax(labschedulingService.insertLabscheduling(labscheduling));
    }

    /**
     * 修改实验室排课表
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:edit')")
    @Log(title = "实验室排课表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Labscheduling labscheduling)
    {
        return toAjax(labschedulingService.updateLabscheduling(labscheduling));
    }

    /**
     * 删除实验室排课表
     */
    @PreAuthorize("@ss.hasPermi('labscheduling:labscheduling:remove')")
    @Log(title = "实验室排课表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(labschedulingService.deleteLabschedulingByIds(ids));
    }
}

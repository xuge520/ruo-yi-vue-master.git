package com.ruoyi.teacherApplication.mapper;

import java.util.List;
import com.ruoyi.teacherApplication.domain.EquipmentRepairsTeacher;

/**
 * 实验室设备报修表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public interface EquipmentRepairsTeacherMapper 
{
    /**
     * 查询实验室设备报修表
     * 
     * @param repairId 实验室设备报修表主键
     * @return 实验室设备报修表
     */
    public EquipmentRepairsTeacher selectEquipmentRepairsTeacherByRepairId(Long repairId);

    /**
     * 查询实验室设备报修表列表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 实验室设备报修表集合
     */
    public List<EquipmentRepairsTeacher> selectEquipmentRepairsTeacherList(EquipmentRepairsTeacher equipmentRepairsTeacher);

    /**
     * 新增实验室设备报修表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 结果
     */
    public int insertEquipmentRepairsTeacher(EquipmentRepairsTeacher equipmentRepairsTeacher);

    /**
     * 修改实验室设备报修表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 结果
     */
    public int updateEquipmentRepairsTeacher(EquipmentRepairsTeacher equipmentRepairsTeacher);

    /**
     * 删除实验室设备报修表
     * 
     * @param repairId 实验室设备报修表主键
     * @return 结果
     */
    public int deleteEquipmentRepairsTeacherByRepairId(Long repairId);

    /**
     * 批量删除实验室设备报修表
     * 
     * @param repairIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEquipmentRepairsTeacherByRepairIds(Long[] repairIds);
}

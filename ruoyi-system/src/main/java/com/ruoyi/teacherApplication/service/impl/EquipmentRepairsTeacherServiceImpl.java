package com.ruoyi.teacherApplication.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.teacherApplication.mapper.EquipmentRepairsTeacherMapper;
import com.ruoyi.teacherApplication.domain.EquipmentRepairsTeacher;
import com.ruoyi.teacherApplication.service.IEquipmentRepairsTeacherService;

/**
 * 实验室设备报修表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@Service
public class EquipmentRepairsTeacherServiceImpl implements IEquipmentRepairsTeacherService 
{
    @Autowired
    private EquipmentRepairsTeacherMapper equipmentRepairsTeacherMapper;

    /**
     * 查询实验室设备报修表
     * 
     * @param repairId 实验室设备报修表主键
     * @return 实验室设备报修表
     */
    @Override
    public EquipmentRepairsTeacher selectEquipmentRepairsTeacherByRepairId(Long repairId)
    {
        return equipmentRepairsTeacherMapper.selectEquipmentRepairsTeacherByRepairId(repairId);
    }

    /**
     * 查询实验室设备报修表列表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 实验室设备报修表
     */
    @Override
    public List<EquipmentRepairsTeacher> selectEquipmentRepairsTeacherList(EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        return equipmentRepairsTeacherMapper.selectEquipmentRepairsTeacherList(equipmentRepairsTeacher);
    }

    /**
     * 新增实验室设备报修表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 结果
     */
    @Override
    public int insertEquipmentRepairsTeacher(EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        return equipmentRepairsTeacherMapper.insertEquipmentRepairsTeacher(equipmentRepairsTeacher);
    }

    /**
     * 修改实验室设备报修表
     * 
     * @param equipmentRepairsTeacher 实验室设备报修表
     * @return 结果
     */
    @Override
    public int updateEquipmentRepairsTeacher(EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        return equipmentRepairsTeacherMapper.updateEquipmentRepairsTeacher(equipmentRepairsTeacher);
    }

    /**
     * 批量删除实验室设备报修表
     * 
     * @param repairIds 需要删除的实验室设备报修表主键
     * @return 结果
     */
    @Override
    public int deleteEquipmentRepairsTeacherByRepairIds(Long[] repairIds)
    {
        return equipmentRepairsTeacherMapper.deleteEquipmentRepairsTeacherByRepairIds(repairIds);
    }

    /**
     * 删除实验室设备报修表信息
     * 
     * @param repairId 实验室设备报修表主键
     * @return 结果
     */
    @Override
    public int deleteEquipmentRepairsTeacherByRepairId(Long repairId)
    {
        return equipmentRepairsTeacherMapper.deleteEquipmentRepairsTeacherByRepairId(repairId);
    }
}

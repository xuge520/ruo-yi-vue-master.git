-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期', '3', '1', 'semester', 'system/semester/index', 1, 0, 'C', '0', '0', 'system:semester:list', '#', 'admin', sysdate(), '', null, '学期菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:semester:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:semester:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:semester:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:semester:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学期导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:semester:export',       '#', 'admin', sysdate(), '', null, '');
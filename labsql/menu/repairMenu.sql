-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修', '3', '1', 'repair', 'system/repair/index', 1, 0, 'C', '0', '0', 'system:repair:list', '#', 'admin', sysdate(), '', null, '设备维修菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:repair:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:repair:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:repair:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:repair:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('设备维修导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:repair:export',       '#', 'admin', sysdate(), '', null, '');
/*
 Navicat Premium Data Transfer

 Source Server         : ruoyi
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : ruoyi

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 25/04/2024 22:42:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lab_schedule
-- ----------------------------
DROP TABLE IF EXISTS `lab_schedule`;
CREATE TABLE `lab_schedule`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '排课学期',
  `lab_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '实验室名称',
  `lab_id` int(0) NULL DEFAULT NULL COMMENT '实验室编号',
  `session` varchar(0) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '节次，如“1-2”，“3-5”',
  `course_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `tname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '任课教师',
  `t_id` int(0) NULL DEFAULT NULL COMMENT '教师id',
  `u_class` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上课班级',
  `day` int(0) NULL DEFAULT NULL COMMENT '星期几',
  `begin` int(0) NULL DEFAULT NULL COMMENT '起始周',
  `end` int(0) NULL DEFAULT NULL COMMENT '结束周',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

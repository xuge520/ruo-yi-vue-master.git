/*
 Navicat Premium Data Transfer

 Source Server         : ruoyi
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : ruoyi

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 25/04/2024 22:42:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for s_application
-- ----------------------------
DROP TABLE IF EXISTS `s_application`;
CREATE TABLE `s_application`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请学期；默认当前学期',
  `s_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请学生姓名',
  `s_id` int(0) NULL DEFAULT NULL COMMENT '申请学生id',
  `week` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请周次',
  `day` int(0) NULL DEFAULT NULL COMMENT '星期几',
  `session` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请节次',
  `lab_id` int(0) NULL DEFAULT NULL COMMENT '实验室编号；根据前者检查是否可用',
  `reason` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请原因',
  `date` datetime(0) NULL DEFAULT NULL COMMENT '填报日期',
  `status` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '申请状态；0为未审核，1为通过，2为驳回，3为使用完毕',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_application
-- ----------------------------
INSERT INTO `s_application` VALUES (1, '2023', '张三', 123, '2', NULL, '3-5', 401, '我的撒对哇对哇上的福娃的发放时代风格是公认的好途径风格迥异繁荣的告诉我股份过户如法国和女子', '2024-04-22 00:00:00', NULL);
INSERT INTO `s_application` VALUES (2, '2024', '张三', 2021, '9', NULL, '6', 402, '213', '2024-04-30 00:00:00', NULL);

SET FOREIGN_KEY_CHECKS = 1;

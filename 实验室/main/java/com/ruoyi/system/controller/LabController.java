package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Lab;
import com.ruoyi.system.service.ILabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验室Controller
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@RestController
@RequestMapping("/system/lab")
public class LabController extends BaseController
{
    @Autowired
    private ILabService labService;

    /**
     * 查询实验室列表
     */
    @PreAuthorize("@ss.hasPermi('system:lab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Lab lab)
    {
        startPage();
        List<Lab> list = labService.selectLabList(lab);
        return getDataTable(list);
    }

    /**
     * 导出实验室列表
     */
    @PreAuthorize("@ss.hasPermi('system:lab:export')")
    @Log(title = "实验室", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Lab lab)
    {
        List<Lab> list = labService.selectLabList(lab);
        ExcelUtil<Lab> util = new ExcelUtil<Lab>(Lab.class);
        util.exportExcel(response, list, "实验室数据");
    }

    /**
     * 获取实验室详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:lab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(labService.selectLabById(id));
    }

    /**
     * 新增实验室
     */
    @PreAuthorize("@ss.hasPermi('system:lab:add')")
    @Log(title = "实验室", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Lab lab)
    {
        return toAjax(labService.insertLab(lab));
    }

    /**
     * 修改实验室
     */
    @PreAuthorize("@ss.hasPermi('system:lab:edit')")
    @Log(title = "实验室", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Lab lab)
    {
        return toAjax(labService.updateLab(lab));
    }

    /**
     * 删除实验室
     */
    @PreAuthorize("@ss.hasPermi('system:lab:remove')")
    @Log(title = "实验室", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(labService.deleteLabByIds(ids));
    }
}

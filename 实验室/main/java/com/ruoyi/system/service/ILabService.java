package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Lab;

/**
 * 实验室Service接口
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public interface ILabService 
{
    /**
     * 查询实验室
     * 
     * @param id 实验室主键
     * @return 实验室
     */
    public Lab selectLabById(Long id);

    /**
     * 查询实验室列表
     * 
     * @param lab 实验室
     * @return 实验室集合
     */
    public List<Lab> selectLabList(Lab lab);

    /**
     * 新增实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    public int insertLab(Lab lab);

    /**
     * 修改实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    public int updateLab(Lab lab);

    /**
     * 批量删除实验室
     * 
     * @param ids 需要删除的实验室主键集合
     * @return 结果
     */
    public int deleteLabByIds(Long[] ids);

    /**
     * 删除实验室信息
     * 
     * @param id 实验室主键
     * @return 结果
     */
    public int deleteLabById(Long id);
}

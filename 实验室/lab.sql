/*
 Navicat Premium Data Transfer

 Source Server         : ruoyi
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : ruoyi

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 27/04/2024 18:17:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lab
-- ----------------------------
DROP TABLE IF EXISTS `lab`;
CREATE TABLE `lab`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '实验室编号',
  `lab_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '实验室名称',
  `lab_type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '实验室类型',
  `e_number` int(0) NULL DEFAULT NULL COMMENT '设备数',
  `manager` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '实验员名称',
  `manage_id` int(0) NULL DEFAULT NULL COMMENT '实验员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lab
-- ----------------------------
INSERT INTO `lab` VALUES (703, '计算机系统实验室', '计算机系统实验室', 62, NULL, NULL);
INSERT INTO `lab` VALUES (704, '计算机系统实验室', '计算机系统实验室', 61, NULL, NULL);
INSERT INTO `lab` VALUES (705, '计算机系统实验室', '计算机系统实验室', 62, NULL, NULL);
INSERT INTO `lab` VALUES (706, '计算机硬件实验室', '计算机硬件实验室', 46, NULL, NULL);
INSERT INTO `lab` VALUES (707, '计算机硬件实验室', '计算机硬件实验室', 43, NULL, NULL);
INSERT INTO `lab` VALUES (732, '软件实验室', '软件实验室', 51, NULL, NULL);
INSERT INTO `lab` VALUES (733, '软件实验室', '软件实验室', 53, NULL, NULL);
INSERT INTO `lab` VALUES (734, '软件实验室', '软件实验室', 52, NULL, NULL);
INSERT INTO `lab` VALUES (735, '软件实验室', '软件实验室', 51, NULL, NULL);
INSERT INTO `lab` VALUES (832, '计算机网络实验室', '计算机网络实验室', 67, NULL, NULL);
INSERT INTO `lab` VALUES (834, '计算机网络实验室', '计算机网络实验室', 68, NULL, NULL);
INSERT INTO `lab` VALUES (835, '物联网实验室', '物联网实验室', 81, NULL, NULL);
INSERT INTO `lab` VALUES (836, '物联网实验室', '物联网实验室', 82, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;

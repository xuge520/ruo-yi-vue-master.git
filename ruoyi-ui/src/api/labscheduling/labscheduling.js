import request from '@/utils/request'

// 查询实验室排课表列表
export function listLabscheduling(query) {
  return request({
    url: '/labscheduling/labscheduling/list',
    method: 'get',
    params: query
  })
}

export function getcourse(query) {
  return request({
    url: '/labscheduling/labscheduling/getcourse',
    method: 'get',
    params: query
  })
}

export function getis(query) {
  return request({
    url: '/labscheduling/labscheduling/getis',
    method: 'get',
    params: query
  })
}


// 查询实验室排课表详细
export function getLabscheduling(id) {
  return request({
    url: '/labscheduling/labscheduling/' + id,
    method: 'get'
  })
}



// 新增实验室排课表
export function addLabscheduling(data) {
  return request({
    url: '/labscheduling/labscheduling',
    method: 'post',
    data: data
  })
}

// 修改实验室排课表
export function updateLabscheduling(data) {
  return request({
    url: '/labscheduling/labscheduling',
    method: 'put',
    data: data
  })
}

// 删除实验室排课表
export function delLabscheduling(id) {
  return request({
    url: '/labscheduling/labscheduling/' + id,
    method: 'delete'
  })
}

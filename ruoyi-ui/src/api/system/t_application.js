import request from '@/utils/request'

// 查询教师排课申请列表
export function listT_application(query) {
  return request({
    url: '/system/t_application/list',
    method: 'get',
    params: query
  })
}

// 查询教师排课申请详细
export function getT_application(id) {
  return request({
    url: '/system/t_application/' + id,
    method: 'get'
  })
}

// 新增教师排课申请
export function addT_application(data) {
  return request({
    url: '/system/t_application',
    method: 'post',
    data: data
  })
}

// 修改教师排课申请
export function updateT_application(data) {
  return request({
    url: '/system/t_application',
    method: 'put',
    data: data
  })
}

// 删除教师排课申请
export function delT_application(id) {
  return request({
    url: '/system/t_application/' + id,
    method: 'delete'
  })
}

package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.StudentApplication;
import com.ruoyi.system.service.IStudentApplicationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生实验室申请Controller
 * 
 * @author lin
 * @date 2024-04-27
 */
@RestController
@RequestMapping("/system/application")
public class StudentApplicationController extends BaseController
{
    @Autowired
    private IStudentApplicationService studentApplicationService;

    /**
     * 查询学生实验室申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentApplication studentApplication)
    {
        startPage();
        List<StudentApplication> list = studentApplicationService.selectStudentApplicationList(studentApplication);
        return getDataTable(list);
    }

    /**
     * 导出学生实验室申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:export')")
    @Log(title = "学生实验室申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentApplication studentApplication)
    {
        List<StudentApplication> list = studentApplicationService.selectStudentApplicationList(studentApplication);
        ExcelUtil<StudentApplication> util = new ExcelUtil<StudentApplication>(StudentApplication.class);
        util.exportExcel(response, list, "学生实验室申请数据");
    }

    /**
     * 获取学生实验室申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:application:query')")
    @GetMapping(value = "/{applicationId}")
    public AjaxResult getInfo(@PathVariable("applicationId") Long applicationId)
    {
        return success(studentApplicationService.selectStudentApplicationByApplicationId(applicationId));
    }

    /**
     * 新增学生实验室申请
     */
    @PreAuthorize("@ss.hasPermi('system:application:add')")
    @Log(title = "学生实验室申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentApplication studentApplication)
    {
        return toAjax(studentApplicationService.insertStudentApplication(studentApplication));
    }

    /**
     * 修改学生实验室申请
     */
    @PreAuthorize("@ss.hasPermi('system:application:edit')")
    @Log(title = "学生实验室申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentApplication studentApplication)
    {
        return toAjax(studentApplicationService.updateStudentApplication(studentApplication));
    }

    /**
     * 删除学生实验室申请
     */
    @PreAuthorize("@ss.hasPermi('system:application:remove')")
    @Log(title = "学生实验室申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{applicationIds}")
    public AjaxResult remove(@PathVariable Long[] applicationIds)
    {
        return toAjax(studentApplicationService.deleteStudentApplicationByApplicationIds(applicationIds));
    }
}
